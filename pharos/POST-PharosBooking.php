<?php 

/**
 * Add Pharos Booking - Creates a booking with Pharos and assigns to booking if booking ID is supplied
 * 
 */


$name = "Test and Live Access Points provided by TAS Solutions goes here";
$vendorKey = "Vendor Key provided by TAS Solutions goes here";

if ($_SERVER['HTTPS'] != null && $_SERVER['HTTPS'] != "")
{
	$protocol = "https://";
} else {
	$protocol = "http://";
}

$serverName = $protocol.$name."/";

ini_set("display_errors","1");

$service_url = $serverName."Login/{$vendorKey}";
$curl = curl_init($service_url);

curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$curl_response = curl_exec($curl);
curl_close($curl);

$json = json_decode($curl_response);

$sessionID = $json->SID;

$url = $serverName.'PharosBooking/'.$sessionID;

$curl_post_data = array(
		
		"departDate" => "2015-04-23",
		"departTime" => "0900",
		"portDepart" => "PME",
		"portArrive" => "JPO",
		"supplierID" => "CDT",
		"returnDepartDate" => "2015-04-23",
		"returnDepartTime" => "2120",
		"returnPortDepart"	=> "JPO",
		"returnPortArrive" => "PME",
		"bookingRef"	=> "TEST124",
		"topicPlusBookingID" => "12007",
		"topicPlusSupplierID" => "CONDOR",
		"fareCode"	=> "IT",
		"passengerNumbers" => array(
			array(
				"passengerType" => "A",
				"passengerQuantity" => "2"
			),
		),
		"passengerDetails" => array(
			array(
				"surname" => "TEST",
				"title" => "Mr",
				"forename" => "OF",
				"type" => "A",
				"nationality" => "UK",
				"sex" => "M",
				"dateOfBirth" => "1981-10-29"
			),
			array(
					"surname" => "TEST",
					"title" => "Mr",
					"forename" => "OF",
					"type" => "A",
					"nationality" => "UK",
					"sex" => "M",
					"dateOfBirth" => "1981-10-29"
			),
			
		),
		"accommodationNumbers" => array(
			array(
				"accomCode" => "REC",
				"accomQuantity" => "2"
			),
			
		),
		
		"vehicleDetails" => array(
			array(
				"type" => "CAR",
				"length" => 600,
				"registration" => "blah",
				"height" => 225,
				"width" => "",
				"makeModel" => ""
					
			)
		)


);


$data_string = json_encode($curl_post_data);

$ch = curl_init($url);  
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
'Content-Type: application/json',
'Content-Length: ' . strlen($data_string))
);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$curl_response = curl_exec($ch);
curl_close($ch);

var_dump($curl_response);

echo "<pre>";
var_dump (json_decode($curl_response));
echo "</pre>";
exit;




?>