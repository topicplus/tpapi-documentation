<?php 

/**
 * Retrieves sailing times based on supplied parameters and optionally checks for availability based on supplied parameters
 * 
 */


$name = "Test and Live Access Points provided by TAS Solutions goes here";
$vendorKey = "Vendor Key provided by TAS Solutions goes here";

if ($_SERVER['HTTPS'] != null && $_SERVER['HTTPS'] != "")
{
	$protocol = "https://";
} else {
	$protocol = "http://";
}

$serverName = $protocol.$name."/";

ini_set("display_errors","1");

$service_url = $serverName."Login/{$vendorKey}";
$curl = curl_init($service_url);

curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$curl_response = curl_exec($curl);
curl_close($curl);

$json = json_decode($curl_response);

$sessionID = $json->SID;

$url = $serverName.'PharosSailingTimes/'.$sessionID;

$curl_post_data = array(
		"departDate" => "2015-06-16",
		"portArrive" => "JPO",
		"portDepart" => "PME",
		"supplierID" => "CDT",
		"availability" => 
			array(
				"fareCode" => "ITX",
				"passengerNumbers" => array(
					array("passengerType" => "A", "passengerQuantity" => 2)
						
				),
				"accommodationNumbers" => array(),
				"vehicleDetails" => array(
					array("type" => "CAR", "length" => 550, "registration" => "")
				),
				"passengerDetails" => array(
					array("surname" => "dddd","title"=> "ddddd", "forename" => "dddd")
				)
				
					
			)
);



$data_string = json_encode($curl_post_data);

$ch = curl_init($url);  
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
'Content-Type: application/json',
'Content-Length: ' . strlen($data_string))
);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$curl_response = curl_exec($ch);
curl_close($ch);

var_dump($curl_response);

echo "<pre>";
var_dump (json_decode($curl_response));
echo "</pre>";
exit;





?>