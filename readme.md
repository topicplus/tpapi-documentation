# tpAPI Sample Code Repository

Sample code for access Topic+ tpAPI service. You must have a valid license for Topic+ and have been given API endpoints and access keys to make use of this code

For more info please contact sales@tas-solutions.co.uk / http://ww.tas-solutions.co.uk
